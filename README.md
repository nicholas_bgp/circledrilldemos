# Circle Drill Demo

Part of the Borilo marching music software project, Circle Drill Demo attempts to simulate marching conditions within simple circle drill exercises to evaluate the effectiveness of tecnhniques such as Strict Dot to Dot marching, attempting to mainting spacing left to right, and using designated poles to push the circle around them and prevent global interval variations.  Random fluctuations in step size and path will be added.

# Block Drill Demo

Added onto this project is a Block Drill Demo which seeks to do the same kinds of things as we did with circle drill. Some of these simulations will include parade block marching, box drill, fan turns, among others.

# Considerations

This project originated and was developed by myself because I believe there's a lot of analysis to do within the marching arts that could benefit it as an activity and give teachers the information they need to select the best tools for their students.

The project is written in MATLAB/Octave (I run Octave on my machine).  It is written so as to run on both platforms.  Everything is a little rough right now as I've just started going.  Any help, as always, makes the world a better place.
