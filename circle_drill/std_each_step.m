function std_each_step (plotnum, intervals, plot_title)
  % figure;
  s_index = 0;
  s_start = [];
  for l = 1:size(intervals, 1)
    s_start = [s_start (s_index + 1)];
    for s = 1:size(intervals, 2)
      s_index = s_index + 1;
      std_intervals_each_step(s_index, 1) = std(intervals(l, s, :));
    end
  end
  subplot(2, 2, plotnum);
  plot(std_intervals_each_step);
  title(plot_title);
  xlabel('Step Index');
  ylabel('Standard Deviation');
  hold on;
  plot(s_start, std_intervals_each_step(s_start), 'rx')
end