
for kk = 1:length(all_sims)
  sims_temp = cell2mat(all_sims(kk));
  avg_sims = squeeze(mean(sims_temp(1,:,:,:,:),1));
  
  figure;
  subplot(2, 2, 1);
  
  std_each_step(1, squeeze(avg_sims(1,:,:,:)), ['Blind Run ' num2str(size(avg_sims, 4))]);
  std_each_step(2, squeeze(avg_sims(2,:,:,:)), ['Control Left Interval ' num2str(size(avg_sims, 4))]);
  std_each_step(3, squeeze(avg_sims(3,:,:,:)), ['Control Left Interval with Posts ' num2str(size(avg_sims, 4))]);
  std_each_step(4, squeeze(avg_sims(4,:,:,:)), ['Control Right Interval ' num2str(size(avg_sims, 4))]);
  
  figure;
  subplot(2, 2, 1);
  
  std_each_step(1, squeeze(avg_sims(5,:,:,:)), ['Control Right Interval with Posts ' num2str(size(avg_sims, 4))]);
  std_each_step(2, squeeze(avg_sims(6,:,:,:)), ['Maintain Left/Right Interval ' num2str(size(avg_sims, 4))]);
  std_each_step(3, squeeze(avg_sims(7,:,:,:)), ['Maintain Left/Right Interval with Posts ' num2str(size(avg_sims, 4))]);
  std_each_step(4, squeeze(avg_sims(8,:,:,:)), ['Control Left Interval Two Performers Away ' num2str(size(avg_sims, 4))]);
  
  figure;
  subplot(2, 2, 1);
  
  std_each_step(1, squeeze(avg_sims(9,:,:,:)), ['Control Left Interval Two Performers Away with Posts ' num2str(size(avg_sims, 4))]);
  std_each_step(2, squeeze(avg_sims(10,:,:,:)), ['Control Right Interval Two Performers Away ' num2str(size(avg_sims, 4))]);
  std_each_step(3, squeeze(avg_sims(11,:,:,:)), ['Control Right Interval Two Performers Away with Posts ' num2str(size(avg_sims, 4))]);
end