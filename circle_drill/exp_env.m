%% IDEA:
% On transitions, step sizes sometimes go crazy for a certain number of steps.
% We model it here with an exponential decay that bottoms at the errorVal,
% normalized to 1.

function [trans_env] = exp_env(recovery_time, steps)
  % If there's no fluctuation with transition, then don't do anything!
  % Send back all ones.
  if recovery_time == 0
    trans_env = ones(1, steps + 1);
  else
    % set up exponential transition envelopes:
    % One side mirrors the other.
    trans_env = [exp(-(0:steps/2-1)/recovery_time) fliplr(exp(-(0:steps/2)/recovery_time))];
    
    % Normalize so the -3dB error is equal to 1:
    trans_env = (1/trans_env(recovery_time + 1))*trans_env;

    % Show the exponential curve for those who care:
%    figure;
%    plot(trans_env);
%    ylabel('Normalized Envelope (*error)');
%    xlabel('Steps');
%    title('Transition Envelope');
%    ylim([0 ceil(max(trans_env))]);
  end
end