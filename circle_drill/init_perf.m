%% IDEA:
% Create the initial circle of performers!
% Note that it won't work if we're not at the origin.  With all the rotations
% we do, the numerical errors in Octave make it too much when we use a nonzeros
% origin!

function [perf_coords r] = init_perf(num_perfs, dist_between_perfs)
  N = linspace(0, 2*pi, num_perfs + 1); % create one extra perf we'll drop!
  N = N(1:end-1); % drop the last perf, as he is at 2*pi = 0! (doubled)

  % Let me draw you a picture...
  % Consider 2 performers (perf1, perf2) on the circle, d is the straight line
  % distance between them, 'a' is the angle indicated, r is the radius of the
  % circle.
  
%  y
%  |             
%  |          perf1 
%  |         _/  \ d
%  |      r_/     |
%  |     _/      perf1
%  |   _/     ___/
%  | _/   ___/    
%  |/a___/       
%  -----perf1---------------------------x


  % Split the triangle the origin, perf1, perf2 make (hard to see with ascii)
  % and then you get this triangle:
%              __
%           __/  | 
%       r__/     |
%     __/        | d/2
%  __/ a/2       |
%  ---------------
  
  % Do some trig, and you get:
  % sin(a/2) = (d/2)/r => r = d/(2*sin(a/2))
  % Which is here:
  r = dist_between_perfs/(2*sin(pi/num_perfs));

  % Create a circle of performers, store coordinates in complex numbers:
  perf_x = r.*cos(N);
  perf_y = r.*sin(N);
  perf_coords = perf_x + 1j*perf_y;  
end