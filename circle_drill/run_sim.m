
function [intervals] = run_sim ( seed,     ...
                 avg_variations,           ...
                 num_perfs,                ...
                 anim,                     ...
                 instructions,             ...
                 errorVal,                 ...
                 recovery_time,            ...
                 bpm,                      ...
                 num_poles,                ...
                 dist_between_perfs,       ...
                 l_adjust,                 ...
                 r_adjust,                 ...
                 lr_adjust,                ...
                 l_two_adjust,             ...
                 r_two_adjust,             ...
                 posts_adjust,             ...
                 accross_cir_adjust)
                 
  % Seed the rand function:
  rand("seed", seed);  
   
  [perf_coords r] = init_perf(num_perfs, dist_between_perfs);
  [t step_size] = init_anim(bpm, num_perfs, dist_between_perfs);

  % Create the post indices:
  post_perfs_indices = round(linspace(1, num_perfs, num_poles + 1));
  post_perfs_indices = post_perfs_indices(1:end - 1);


  if anim
    figure;
  end

  for l = 1:length(instructions)
    steps = instructions(l);
    trans_env = exp_env(recovery_time, abs(steps));
    
    for n = 0:(abs(steps)-1)
      if anim
        drawnow;
        clf;
        grid on;
      end
      
      % Calculate all the rotations:
      for k = 1:num_perfs
        theta = step_size*sign(steps);
        results = rotate_cir(theta)*[real(perf_coords(k)) imag(perf_coords(k)) 1]';
        perf_coords(k) = results(1) + 1j*results(2);
      end
      
      % Create the control performers before any changes are added:
      perf_coords_control = perf_coords;
      
      for k = 1:num_perfs
        % Grab some important coordinates:
        if k < num_perfs
          perf_front = perf_coords(k + 1);
        else
          perf_front = perf_coords(1);
        end
        
        dist_front = abs(perf_coords(k) - perf_front);
        
        if k < (num_perfs - 1)
          perf_two_front = perf_coords(k + 2);
        else
          perf_two_front = perf_coords(num_perfs - k + 1);
        end
        
        dist_two_front = abs(perf_coords(k) - perf_two_front);
        
        if k > 1
          perf_behind = perf_coords(k - 1);
        else
          perf_behind = perf_coords(end);
        end
        
        dist_behind = abs(perf_coords(k) - perf_behind);
        lr_adjust_ideal_location = (perf_front + perf_behind)/2;
        lr_adjust_ideal_location = r*lr_adjust_ideal_location/abs(lr_adjust_ideal_location);      
        
        if k > 2
          perf_two_behind = perf_coords(k - 2);
        else
          perf_two_behind = perf_coords(end - k);
        end
        
        dist_two_behind = abs(perf_coords(k) - perf_two_behind);
        
        % Add in variations:  
        in_out = (rand(1)-1/2) + 1; % Not as much variation in/out of the circle as tangentially
        del_x = 2*(rand(1)-1/2);
        del_y = 2*(rand(1)-1/2);
        
        % avg_variations only adds to tangential component!
        unit_vector = perf_coords(k)/abs(perf_coords(k));
        results = rotate_cir(sign(steps)*pi/2)*[real(unit_vector) imag(unit_vector) 1]';
        unit_tang_vector = results(1) + 1j*results(2);      
        
        % info for only left or only right interval control:
        if l_adjust
          l_interval(n + 1, k) = dist_behind;
          l_adjust_ideal_location = (perf_behind + sign(steps)*unit_tang_vector*mean(l_interval(:, k)));
          l_adjust_ideal_location = r*l_adjust_ideal_location/abs(l_adjust_ideal_location);
        end
        if r_adjust
          r_interval(n + 1, k) = dist_front;
          r_adjust_ideal_location = (perf_front - sign(steps)*unit_tang_vector*mean(r_interval(:, k)));
          r_adjust_ideal_location = r*r_adjust_ideal_location/abs(r_adjust_ideal_location);
        end
        
        % info for two donw left or right interval control:
        if l_two_adjust
          l_two_interval(n + 1, k) = dist_two_behind;
          
          unit_two_vector = (perf_coords(k) - perf_two_behind)/abs(perf_coords(k) - perf_two_behind);
          
          l_two_adjust_ideal_location = (perf_two_behind + unit_two_vector*mean(l_two_interval(:, k)));
          l_two_adjust_ideal_location = r*l_two_adjust_ideal_location/abs(l_two_adjust_ideal_location);
        end
        if r_two_adjust
          r_two_interval(n + 1, k) = dist_two_front;
          
          unit_two_vector = (perf_coords(k) - perf_two_front)/abs(perf_coords(k) - perf_two_front);
          
          r_two_adjust_ideal_location = (perf_two_front + unit_two_vector*mean(r_two_interval(:, k)));
          r_two_adjust_ideal_location = r*r_two_adjust_ideal_location/abs(r_two_adjust_ideal_location);
        end
        
        % Start adding in the adjustments if we need any!
        adjustments(k) = 0;
        variations(k) = .01*errorVal*in_out*(del_x + del_y*1j)*trans_env(n + 1) + unit_tang_vector*avg_variations(k);
        
        if lr_adjust
          adjustments(k) = adjustments(k) + (lr_adjust_ideal_location - perf_coords(k))/2;
        end
        
        if l_adjust
          adjustments(k) = adjustments(k) + (l_adjust_ideal_location - perf_coords(k))/2;
        end
        
        if r_adjust
          adjustments(k) = adjustments(k) + (r_adjust_ideal_location - perf_coords(k))/2;
        end
        
        if l_two_adjust
          adjustments(k) = adjustments(k) + (l_two_adjust_ideal_location - perf_coords(k))/2;
        end
        
        if r_two_adjust
          adjustments(k) = adjustments(k) + (r_two_adjust_ideal_location - perf_coords(k))/2;
        end
        
        if posts_adjust
          % Make sure the posts do their best to get back to their posts!
          if any(k == post_perfs_indices)
            % We assume the only adjustments will be to get back to where they
            % should be:
            adjustments(k) = (perf_coords_control(k) - perf_coords(k));
            
            % We further assume they will have a lower variation in step:
            variations(k) = .01*errorVal*in_out*(del_x + del_y*1j)/4;
          end
        end
      
        perf_coords(k) = perf_coords(k) + variations(k) + adjustments(k);

        % Save the intervals between performers:
        intervals(l, n + 1, k) = dist_front;
        
        % Save the radii of the performers:
        radii(n + 1, k) = abs(perf_coords(k));

      end
      
      % Save the the distance the actual performers differed from the control:
      displacement_from_control(n + 1, :) = perf_coords_control - perf_coords;
      
      % Set up the posts:
      if posts_adjust && anim
        % choose 4 equally spaced performers to be the posts.  They will do
        % their best to reach their posts on transitions.
        for m = post_perfs_indices
          hold on;
          plot([0 real(perf_coords(m))], [0 imag(perf_coords(m))], '-x');
        end
      elseif anim
        % Add a tracer, so we know which way the circle spins!
          hold on;
          plot([0 real(perf_coords(1))], [0 imag(perf_coords(1))], '-x');
      end
      
      % Look at the tang_vector:
      % plot([real(perf_coords(1)) real(perf_coords(1) + unit_tang_vector)], [imag(perf_coords(1)) imag(perf_coords(1) + unit_tang_vector)], '-');
      
      if anim
        plot(real(perf_coords), imag(perf_coords), 'ko');
        title('Circle Drill Simulation');
        pause(t);
      end
    end
  end
end