%% IDEA:
% Send back how much time a beat takes (how much we'll have to wait each step
% during animation), and the step size, converted to radian angle.

function [t step_size] = init_anim(bpm, num_perfs, ideal)
  t = 60/bpm; % Convert bpm to time per beat
  step_size = (2*pi/num_perfs)/ideal;
end