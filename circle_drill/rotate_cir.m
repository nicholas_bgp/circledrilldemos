%% IDEA:
% There's probably a matlab function that does this, but it was just as easy
% to make it.

function [rot_mat] = rotate_cir(theta)
  % Now plug in the theta to the rotation matrix:
  rot_mat = [cos(theta) -sin(theta) 0; sin(theta) cos(theta) 0; 0 0 1];
end