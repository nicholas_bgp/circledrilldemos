%% Background:
% Marching band techniques are infamously learned and passed on as an oral
% tradition.  Often these techniques are beneficial, sometimes it is unclear.
% There are virtually no analyses that I am aware of that compares the
% merits of thse various techniques or efforts to find an optimal solution
% based on a) dot-mongering and/or b) form control.  Much of visual rehearsal
% is spent learning and applying these techniques for use within the show
% to maximize on-field shape control and dot accuracy.  If these techniques
% are somehow not having the intended effects, a lot of time could be saved
% in rehearsal and new techniques could be developed which work more
% effectively.

%% Goal of this project:
% Compare several existing techniques for groups of varying sizes,
% performance levels, and intended outcomes (correct dot or shape control).
% Make recommendations about the merit of each technique for these groups
% and ways these techniques might be improved or other techniques that
% might prove to more effectively preserve shape/dot accuracy.

clear;
close all;

anim = false;
instructions = [ 16 -16 16 -16 ]; % Adjust to include in and out of the circle movement
errorVal = 7;
sigma = 0.02; % accounts for clumping
recovery_time = floor(errorVal/2); % accounts for more errors in transitions
bpm = 180;
num_poles = 4;
dist_between_perfs = 2;

% Simulation Parameters:
l_adjust = false; % look left and try to maintain average interval over move
r_adjust = false; % Similarly for right
lr_adjust = false;
l_two_adjust = false; % Look two down and adjust.  Will produce rigid graph with odd number of performers
r_two_adjust = false;
posts_adjust = false;
accross_cir_adjust = false; % look at the person directly accross from you (or
                            % for odd numbers, split the space across from you)
rand_reps = 100;
num_perfs_space = 25:5:50;

% START SIMULATIONS:

for jj = 1:numel(num_perfs_space)
  num_perfs = num_perfs_space(jj);
  sims = zeros(rand_reps, 2, numel(instructions), max(abs(instructions)), num_perfs);
  for ii = 1:rand_reps
    % Generate all the random stuff here so it's consistent through all the simulations!
    seed = ii; % Get a new seed every run through!
    avg_variations = normrnd(0, sigma, 1, num_perfs); % Get a new set of variations for performers every time!
    

    % Run blind:
    sim = run_sim_optimized (seed,avg_variations,num_perfs,anim,instructions,errorVal,recovery_time,bpm,num_poles,dist_between_perfs,l_adjust,r_adjust,lr_adjust,l_two_adjust,r_two_adjust,posts_adjust,accross_cir_adjust);
    sims(ii,1,:,:,:) = sim;

    % Run with l_adjust:
    l_adjust = true;
    sim = run_sim_optimized (seed,avg_variations,num_perfs,anim,instructions,errorVal,recovery_time,bpm,num_poles,dist_between_perfs,l_adjust,r_adjust,lr_adjust,l_two_adjust,r_two_adjust,posts_adjust,accross_cir_adjust);
    sims(ii,2,:,:,:) = sim;
    l_adjust = false;
    
    % Run with l_adjust and posts:
    l_adjust = true;
    posts_adjust = true;
    sim = run_sim_optimized (seed,avg_variations,num_perfs,anim,instructions,errorVal,recovery_time,bpm,num_poles,dist_between_perfs,l_adjust,r_adjust,lr_adjust,l_two_adjust,r_two_adjust,posts_adjust,accross_cir_adjust);
    sims(ii,3,:,:,:) = sim;
    l_adjust = false;
    posts_adjust = false;
    
    % Run with r_adjust:
    r_adjust = true;
    sim = run_sim_optimized (seed,avg_variations,num_perfs,anim,instructions,errorVal,recovery_time,bpm,num_poles,dist_between_perfs,l_adjust,r_adjust,lr_adjust,l_two_adjust,r_two_adjust,posts_adjust,accross_cir_adjust);
    sims(ii,4,:,:,:) = sim;
    r_adjust = false;
    

    % Run with r_adjust and posts:
    r_adjust = true;
    posts_adjust = true;
    sim = run_sim_optimized (seed,avg_variations,num_perfs,anim,instructions,errorVal,recovery_time,bpm,num_poles,dist_between_perfs,l_adjust,r_adjust,lr_adjust,l_two_adjust,r_two_adjust,posts_adjust,accross_cir_adjust);
    sims(ii,5,:,:,:) = sim;
    r_adjust = false;
    posts_adjust = false;
    
    % Run with lr_adjust:
    lr_adjust = true;
    sim = run_sim_optimized (seed,avg_variations,num_perfs,anim,instructions,errorVal,recovery_time,bpm,num_poles,dist_between_perfs,l_adjust,r_adjust,lr_adjust,l_two_adjust,r_two_adjust,posts_adjust,accross_cir_adjust);
    sims(ii,6,:,:,:) = sim;
    lr_adjust = false;

    % Run with lr_adjust and posts:
    lr_adjust = true;
    posts_adjust = true;
    sim = run_sim_optimized (seed,avg_variations,num_perfs,anim,instructions,errorVal,recovery_time,bpm,num_poles,dist_between_perfs,l_adjust,r_adjust,lr_adjust,l_two_adjust,r_two_adjust,posts_adjust,accross_cir_adjust);
    sims(ii,7,:,:,:) = sim;
    lr_adjust = false;
    posts_adjust = false;
    
    % Run with l_two_adjust:
    l_two_adjust = true;
    sim = run_sim_optimized (seed,avg_variations,num_perfs,anim,instructions,errorVal,recovery_time,bpm,num_poles,dist_between_perfs,l_adjust,r_adjust,lr_adjust,l_two_adjust,r_two_adjust,posts_adjust,accross_cir_adjust);
    sims(ii,8,:,:,:) = sim;
    l_two_adjust = false;

    % Run with l_two_adjust with posts:
    l_two_adjust = true;
    posts_adjust = true;
    sim = run_sim_optimized (seed,avg_variations,num_perfs,anim,instructions,errorVal,recovery_time,bpm,num_poles,dist_between_perfs,l_adjust,r_adjust,lr_adjust,l_two_adjust,r_two_adjust,posts_adjust,accross_cir_adjust);
    sims(ii,9,:,:,:) = sim;
    l_two_adjust = false;
    posts_adjust = false;
    
    % Run with r_two_adjust:
    r_two_adust = true;
    sim = run_sim_optimized (seed,avg_variations,num_perfs,anim,instructions,errorVal,recovery_time,bpm,num_poles,dist_between_perfs,l_adjust,r_adjust,lr_adjust,l_two_adjust,r_two_adjust,posts_adjust,accross_cir_adjust);
    sims(ii,10,:,:,:) = sim;
    r_two_adjust = false;

    % Run with r_two_adjust with posts:
    r_two_adjust = true;
    posts_adjust = true;
    sim = run_sim_optimized (seed,avg_variations,num_perfs,anim,instructions,errorVal,recovery_time,bpm,num_poles,dist_between_perfs,l_adjust,r_adjust,lr_adjust,l_two_adjust,r_two_adjust,posts_adjust,accross_cir_adjust);
    sims(ii,11,:,:,:) = sim;
    r_two_adjust = false;
    posts_adjust = false;
    
    disp(['Finished sim ' num2str(ii)]);
    
  end
  disp(['Finished perf group ' num2str(jj)]);
  all_sims{jj} = sims;
  
end
  
% Look at the avg standard deviation of the intervals for each step:
for kk = 1:length(all_sims)
  sims_temp = cell2mat(all_sims(kk));
  avg_sims = squeeze(mean(sims_temp(1,:,:,:,:),1));
  
  figure;
  subplot(2, 2, 1);
  
  std_each_step(1, squeeze(avg_sims(1,:,:,:)), ['Blind Run ' num2str(size(avg_sims, 4))]);
  std_each_step(2, squeeze(avg_sims(2,:,:,:)), ['Control Left Interval ' num2str(size(avg_sims, 4))]);
  std_each_step(3, squeeze(avg_sims(3,:,:,:)), ['Control Left Interval with Posts ' num2str(size(avg_sims, 4))]);
  std_each_step(4, squeeze(avg_sims(4,:,:,:)), ['Control Right Interval ' num2str(size(avg_sims, 4))]);
  
  figure;
  subplot(2, 2, 1);
  
  std_each_step(1, squeeze(avg_sims(5,:,:,:)), ['Control Right Interval with Posts ' num2str(size(avg_sims, 4))]);
  std_each_step(2, squeeze(avg_sims(6,:,:,:)), ['Maintain Left/Right Interval ' num2str(size(avg_sims, 4))]);
  std_each_step(3, squeeze(avg_sims(7,:,:,:)), ['Maintain Left/Right Interval with Posts ' num2str(size(avg_sims, 4))]);
  std_each_step(4, squeeze(avg_sims(8,:,:,:)), ['Control Left Interval Two Performers Away ' num2str(size(avg_sims, 4))]);
  
  figure;
  subplot(2, 2, 1);
  
  std_each_step(1, squeeze(avg_sims(9,:,:,:)), ['Control Left Interval Two Performers Away with Posts ' num2str(size(avg_sims, 4))]);
  std_each_step(2, squeeze(avg_sims(10,:,:,:)), ['Control Right Interval Two Performers Away ' num2str(size(avg_sims, 4))]);
  std_each_step(3, squeeze(avg_sims(11,:,:,:)), ['Control Right Interval Two Performers Away with Posts ' num2str(size(avg_sims, 4))]);
end
% Look at the 

%sensitivity = (
