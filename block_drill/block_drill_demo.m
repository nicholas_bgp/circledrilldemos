clear;
close all;

% Brad's ideas:
% Get actual data from human groups of different calibers
% Film with drone and track movement of performers and their errors

% Everyone is only responsible for one link, or two links, etc, and see how accurate this becomes
% Is it more efficient to pay attention to more links with less accuracy, or less links with more?
% Simulate very any ways of doing fan/gate turns - we can even show how some of them are impossible


% Params for the block:
cols = 5;
rows = 10;
bpm = 180;
anim = true;

               %  x  y  steps
instructions = [ 
                  0  8  8;
                  0 -8  8;
                  8  0  8;
                 -8  0  8;
               ];

% Initialize performers into a block centered on the origin:
perf_coords = zeros(cols, rows);
for y = 1:rows
  for x = 1:cols
    perf_coords(x, y) = (x + 1j*y) - ((cols + 1)/2 + 1j*(rows + 1)/2);
  end
end

t = 60/bpm;
% This was trickier than I thought it would be...
axis_lims = [ (min(real(perf_coords(:))) - max(abs(min([cumsum(instructions(:, 1)) instructions(:, 1)])))) (max(real(perf_coords(:))) + max(abs(max([cumsum(instructions(:, 1)) instructions(:, 1)])))) ...
              (min(imag(perf_coords(:))) - max(abs(min([cumsum(instructions(:, 2)) instructions(:, 2)])))) (max(imag(perf_coords(:))) + max(abs(max([cumsum(instructions(:, 1)) instructions(:, 2)])))) ];

figure;
title('Block Drill Simulation');
axis(axis_lims);
grid on;

for l = 1:length(instructions)
  vector = instructions(l, 1) + 1j*instructions(l, 2);
  steps = instructions(l, 3);
  for n = 1:steps
    
    % Adjust the coordinates, no variation:
    perf_coords = perf_coords + vector/steps;
    
    % Draw the block:
    cla(gca);
    hold on;
    plot(real(perf_coords), imag(perf_coords), 'ko');
    hold off;
    drawnow;
    
    % Pause if we said we wanted to see it in time:
    if anim
      pause(t);
    end
  end
end