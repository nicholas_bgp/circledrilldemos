%% IDEA:
% Have a function that spits out the data I want to see to the console.

function [distances percent_error] = results_report(num_perfs, perf_coords, ideal)
  for n = 1:num_perfs
    if (n < num_perfs)
      distances(n) = abs(perf_coords(n) - perf_coords(n + 1));
    else
      distances(n) = abs(perf_coords(n) - perf_coords(1));
    end
  end
  
  percent_error = abs(100*(distances - ideal)./distances);

  av_percent_error = mean(percent_error)
  max_percent_error = max(percent_error)
  std_dev = std(percent_error)
  disp(''); % give a buffer blank line in the output
end