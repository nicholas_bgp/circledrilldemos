function [correction_vector] = io_correction(ideal_location, curr_location)
  % So here we go. There will be some distance in/out of the radial spoke
  % that we'll want to adjust.  Consider the set up:
  
  %                   __me_
  %                __/  |  \__
  %             __/     |     \__h
  %          __/      y |        \__
  %       __/           |           \__
  %  perf1-----------------------------perf2
  %                            x
  %
  % We want to find that y correction factor and then send back a scale factor!
%  y_ideal = (dist_between_perfs*sin(step_size);
%  y_correction = (y_ideal - sqrt(h^2 - x^2))/2;
%  x
%  
%  % Now convert the y_correction radial distance to a scale factor:
%  y_correction_scale_factor = 1; % 1 + y_correction/r;

  correction_vector = (curr_location - ideal_location)/2
end