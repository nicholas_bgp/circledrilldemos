%% IDEA:
% Run the simulation and animate the case where performers are trying to
% correct only using the people left and right of them.
% We still have random step size errors.

function [perf_coords distances] = ...
  control_lr_interval_circle(origin, perf_coords, ideal, steps, ...
                             num_perfs, step_size, errorVal, trans_env, ...
                             t, r, dist_between_perfs, anim)
  figure;
  
  % We need to update every performer every step we take:
  for n = 0:steps
    drawnow;
    plot(real(perf_coords), imag(perf_coords), 'o');
    title('Circle Drill Simulation (Left/Right Interval Control)');
  
    % Apply the translation/rotation matrix on every performer:
    for k = 1:num_perfs
      
      % Now we have random step fluctuations along with the transition envelope,
      % AND we now correct for intervals looking looking left to right!
      % ALSO: correct in/out of the circle by looking left to reft in the circle.
    
      % Find out how far in front the guy infront of you is:
      if k < num_perfs
        dist_front = abs(perf_coords(k) - perf_coords(k + 1));
        perf_front = perf_coords(k + 1);
      else
        dist_front = abs(perf_coords(k) - perf_coords(1));
        perf_front = perf_coords(1);
      end
      % Find out how far behind the guy behind you is:
      if k > 1
        dist_behind = abs(perf_coords(k) - perf_coords(k - 1));
        % x_times_2 = abs(perf_front - perf_coords(k - 1));
        % x = real(perf_coords(k)) - real(perf_coords(k - 1));
        
        ideal_location = (perf_front - perf_coords(k - 1))/2;
        
      else
        dist_behind = abs(perf_coords(k) - perf_coords(end));
        % x_times_2 = abs(perf_front - perf_coords(end));
        % x = real(perf_coords(k)) - real(perf_coords(end));
        
        ideal_location = (perf_front - perf_coords(end))/2;
      end
      
      % Normalize the ideal tangential location:
      ideal_location = r*ideal_location/abs(ideal_location);
      
      ideal_location - perf_coords(k)
    
      % This the angle of rotation (step) around the direction of the circle.
      % Notice both the standatd step size error and then a correction term!
      theta = step_size + std_step_size_error(errorVal, step_size, trans_env(n + 1)) ...
                        + lr_correction(dist_front, dist_behind, r, errorVal, trans_env(n + 1));
      
      % Perform the rotation, then store the result:
      results = rotate_cir(origin, theta)*[real(perf_coords(k)) imag(perf_coords(k)) 1]';
      perf_coords(k) = results(1) + 1j*results(2);
      
      % Now we want to move in or out of the radial spoke, simulating a step
      % not taken exactly in the path of the circle:
      % We can do this by scaling the complex number!
%      perf_coords(k) = perf_coords(k) * ...
%                       io_correction(ideal_tangential_location, dist_behind, r, dist_between_perfs, step_size, errorVal, trans_env(n + 1)) * ...
%                       radial_displacement(errorVal, step_size, r, trans_env(n + 1));

      results = rotate_cir(origin, theta)*[real(ideal_location) imag(ideal_location) 1]';
      ideal_location = results(1) + 1j*results(2);
      perf_coords(k) = perf_coords(k); % + io_correction(ideal_location, perf_coords(k));
      
    end

    % Only wait to step if we asked to!
    if anim
      pause(t);
    end
  end
  
  % Find average percent error in intervals over move:
  % --TODO
  
  % Write out the console the results of the simulation!
  disp('Left/Right Interval Control results:');
  [distances percent_error] = results_report(num_perfs, perf_coords, ideal);
  
end