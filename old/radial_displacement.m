%% IDEA:
% Add in a standard radial displacement error (not taking a step in the
% path of the circle)

function [out] = radial_displacement(errorVal, step_size, r, trans_env_val)
  % Convert step_size (in radians) to a radial distance.
  radial_step_size = r*sin(step_size);
  
  % Error is in percentage, so scale by .01.
  % We want a random number between -1/2 and 1/2, so center rand(1) about 0
  % Adjust the step size further using the transition envelope to add more
  % error on transitions and normal after we've taken the number of steps needed
  % to adjust to the new move.
  % The first term (1) comes from the fact that we're passing out a scaling
  % factor to scale the radius by.
  out = 1; % + (.01*errorVal*(rand(1)-1/2)/4*radial_step_size); %*trans_env_val;
end