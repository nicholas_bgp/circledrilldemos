%% IDEA:
% Add a correction angle to theta when computing the roatation matrix to get
% the step size.  Check the distance in the front and in the back, and then
% split that distance and factor in error adjustment.

function [correction] = lr_correction(dist_front, dist_behind, r, errorVal, trans_env_val)
  % Take the distance you see infront and behind and split it:
  difference = (dist_front - dist_behind)/2;
  
  % If the difference is small, don't do anything:
  % Also note we can cover both the + and - cases by using the fact that
  % asin is an odd function!
  if abs(difference) > 1e-3
    % Now, we need an angle to add to theta!
    % Consider the triangle:
        
    %              __
    %           __/  | 
    %       r__/     |
    %     __/        | difference
    %  __/ a         |
    %  ---------------

    % We are trying to find the angle 'a'.
    % Do some trig:
    % sin(a) = difference/r => a = asin(difference/r)
      
    % Also add in error terms (we only want rand(1) from 0->1):
    correction = .01*errorVal*asin(difference/r)*rand(1)*trans_env_val;
  else
    correction = 0;
  end
end