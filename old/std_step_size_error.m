%% IDEA:
% Add in a standard error on every step taken.
% Given in radians.

function [out] = std_step_size_error(errorVal, step_size, trans_env_val)
  % Error is in percentage, so scale by .01.
  % We want a random number between -1 and 1, so center rand(1) about 0 and
  % then multiply by 2 to get the bounds we need.
  % Adjust the step size further using the transition envelope to add more
  % error on transitions and normal after we've taken the number of steps needed
  % to adjust to the new move.
  out = (.01*errorVal*2*(rand(1)-1/2)*step_size)*trans_env_val;
end