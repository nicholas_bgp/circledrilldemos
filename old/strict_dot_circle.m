%% IDEA:
% Run the simulation and animate the case where performers are trying to
% to go blind dot to dot, not adjusting.
% We have random step size errors, but no corrections.

function [perf_coords distances avg_error_during_move] = ...
                strict_dot_circle(origin, perf_coords, ideal, steps, ...
                                  num_perfs, step_size, errorVal, ...
                                  trans_env, t, anim)
  figure;

  % Update each performer every step of the animation:
  for n = 0:steps
    drawnow;
    plot(real(perf_coords), imag(perf_coords), 'o');
    title('Circle Drill Simulation (Strict Dot)');
    
    % Apply the translation/rotation matrix on each performer:
    for k = 1:num_perfs
      
      % Each performer's individual step may be different:
      % - variation in step size
      % - also dependent on which count of the move we're in:
      %     - Step size is more variable on transitions, stabilizes in middle of move
      theta = step_size + std_step_size_error(errorVal, step_size, trans_env(n + 1));
      
      % Calculate the rotation and store the resultant coordinates:
      results = rotate_cir(origin, theta)*[real(perf_coords(k)) imag(perf_coords(k)) 1]';
      perf_coords(k) = results(1) + 1j*results(2);
    end
    
    % during the move, we'd like to average the interval error factor:
    % --TODO
    
    % Only animate if we asked to!
    if anim
      pause(t);
    end
  end
  
  % Find average percent error in intervals over move and print out to console:
  disp('Strict Dot results:');
  [distances percent_error] = results_report(num_perfs, perf_coords, ideal);

end