clear;
close all;

## Nicholas McKibben, 17 March, 2017

%% IDEA:
% Model circle drill in several different ways to determine if different
% theories of marching (strict dot, guiding left-right, guiding accross the
% form, hybrid) yield better interval control.  There will be several parameters:
%    - stepsize error (basically skill of marchers, normal people have ~10% in paces)
%    - transitions (more error on transitions, less in middle of move)

% Create a circle of performers.
% This will serve as our 'control' group that marches perfectly and which we
% will later distort.
num_perfs              = 50;
dist_between_perfs     = 2; % distance between the performers.  Units are arbitrary.
[origin perf_coords r] = init_perf(num_perfs, dist_between_perfs);

% I suppose I should take this opportunity to inform you that I store
% coordinates as imaginary numbers:
%     real part: x
%     imag part: y
% Now you know.

% Init animation params.
% Assume you take a unit of distance between each other each step.
% t is how long in sec each beat is.
bpm = 180;
[t step_size] = init_anim(bpm, num_perfs, dist_between_perfs);
anim = false; % Do we want to see it animated, or breeze right on through?

% Also, step_size is given as a radian angle.  Just so you know.

% Misc. Params.
% What would be cool, is to have directions: 16 forward, 8 back,etc.
% Right now we only spin CW a given number of steps.
% The errorVal is a percent error on each step.  I've read that people pacing
% usually are within 10%.  This number will tell us how experienced our
% marchers are.
steps = 16;
errorVal = 0; % percent

% Get the coordinated for the ideal performers' ending set:
for n = 0:steps
  for k = 1:num_perfs
    results = rotate_cir(origin, step_size)*[real(perf_coords(k)) imag(perf_coords(k)) 1]';
    dist_between_perfs_perf_coords(k) = results(1) + 1j*results(2);
  end
end

%% Exponential transition envelope
% The idea is that at each transition (start, stop, direction change), step
% size goes wonky.  The time constant for the exponential decay is
% recovery_time.  This number is how many steps it takes us to get back to the
% regular step size error (normalized to 1 in the transition envelope).
% All steps before the recovery_time give elevated errors.

recovery_time = 2; % how many steps till we get error under control?
[trans_env] = exp_env(recovery_time, steps); % Normalize and don't let the error go under errorVal

%% Now let's run the simulations!
% We'll run each one and then look at some plots and analyze the information
% at the end of all the simulations.

%% Strict Dot Marching (no checkpoints):
% It would probably be a good idea to cut down the argument lists...
[strict_dot_final_perf_coords strict_dot_final_distances] = ...
            strict_dot_circle(origin, perf_coords, dist_between_perfs, ...
            steps, num_perfs, step_size, errorVal, trans_env, t, anim);
            
%% Control Interval Left-Right (no checkpoints):
[lr_control_final_perf_coords lr_control_final_distances] =  ...
            control_lr_interval_circle(origin, perf_coords,  ...
            dist_between_perfs, steps, num_perfs, step_size, ...
            errorVal, trans_env, t, r, dist_between_perfs, anim);

%% To come...
% > Pathway nonidealities
% > Direction changes (more complex exercises)
% > and more...
            
%% Compare:
% This is showing how accurately the performers got the point on the field they
% were suppposed to get to:
figure;
plot(real(dist_between_perfs_perf_coords), imag(dist_between_perfs_perf_coords), 'o');
hold on;
plot(real(strict_dot_final_perf_coords), imag(strict_dot_final_perf_coords), 'o');
plot(real(lr_control_final_perf_coords), imag(lr_control_final_perf_coords), 'o');
title('Comparison');
legend('Ideal', 'Strict Dot', 'L/R Control');

% Shows relatively how far away the performers ended up from the dist_between_perfs ending points.
% Units right now are meaningless.  Don't try.
figure;
bar([
      mean(abs(strict_dot_final_perf_coords - dist_between_perfs_perf_coords))
      mean(abs(lr_control_final_perf_coords - dist_between_perfs_perf_coords))
    ]);
title('Average Distance off End Dot');

% This is showing how good the circle turned out (spacing):
figure;
plot(1:num_perfs, 2); % plot the dist_between_perfs constant distance
hold on;
plot(strict_dot_final_distances);
plot(lr_control_final_distances);
title('Intervals between performers');
legend('Ideal', 'Strict Dot', 'L/R Control');
xlabel('Performer');
ylabel('Distance to Next Performer');

% Don't forget to check the console to see some more info!